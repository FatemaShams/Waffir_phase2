//
//  NavigationController.swift
//  Waffir
//
//  Created by MacBook Pro on 05/02/2018.
//  Copyright © 2018 confrontstar. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.applyNavigationGradient(colors: [ #colorLiteral(red: 0.03692631423, green: 0.6838768125, blue: 0.6695318818, alpha: 1) , #colorLiteral(red: 0.01694075391, green: 0.4883860946, blue: 0.6220107675, alpha: 1)])

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
