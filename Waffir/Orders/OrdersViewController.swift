//
//  OrdersViewController.swift
//  Waffir
//
//  Created by MacBook Pro on 05/02/2018.
//  Copyright © 2018 confrontstar. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController  , UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[p].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        
        cell.title.text = data[p][indexPath.row]
        
        return cell
        
        
    }
    
    
    
    @IBOutlet weak var SegmentedBar: UISegmentedControl!
    
    @IBOutlet weak var orderTable: UITableView!
    
    var data = [
        ["⚽️ Soccer",       "⛳️ Golf",      "🏀 Basketball",    "🏈 American Football",
         "⚾️ Baseball",     "🎾 Tennis",    "🏐 Valleyball",    "🏸 Badminton"],
        ["🍎 Apple",        "🍐 Pear",      "🍓 Strawberry",    "🥑 Avocado",
         "🍌 Banana",       "🍇 Grape",     "🍈 Melon",         "🍊 Orange",
         "🍑 Peach",        "🥝 Kiwi"] , ["🍎 Apple",        "🍐 Pear",      "🍓 Strawberry",    "🥑 Avocado",
                                              "🍌 Banana",       "🍇 Grape",     "🍈 Melon",         "🍊 Orange",
                                              "🍑 Peach",        "🥝 Kiwi"]
    ]
    
    
    
    var p : Int!
    
    @IBOutlet weak var outerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        outerView.clipsToBounds = true;
        outerView.layer.cornerRadius = 0; //whatever
        outerView.layer.borderWidth = 0.5;
        outerView.layer.borderColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        SegmentedBar.tintColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        p = 2
        
        SegmentedBar.selectedSegmentIndex = 2;

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func Switch(_ sender: UISegmentedControl) {
        
        p = sender.selectedSegmentIndex
        orderTable.reloadData()
        
        
    }
    
    
    
    
    
}
