//
//  CustomColors.swift
//  Waffir
//
//  Created by MacBook Pro on 04/02/2018.
//  Copyright © 2018 confrontstar. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    static let myPink = #colorLiteral(red: 0.8679370284, green: 0.2307879627, blue: 0.426943928, alpha: 1)
    static let myDarkGreen = #colorLiteral(red: 0.03691732138, green: 0.6209536195, blue: 0.6105498672, alpha: 1)
    static let myLightGreen = #colorLiteral(red: 0.5137733221, green: 0.8364477158, blue: 0.8298620582, alpha: 1)
}


