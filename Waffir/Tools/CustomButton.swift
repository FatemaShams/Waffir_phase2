//
//  CustomButton.swift
//  AMDA
//
//  Created by Abdalrahim Abdulah on 29/01/2018.
//  Copyright © 2018 Tima_Shams. All rights reserved.
//

import UIKit

/// Designable class for Button
@IBDesignable class CustomButton : UIButton {
    
    /// Custom the corner radius
    @IBInspectable var cornerRadius : Int {
        set {
            self.layer.cornerRadius = CGFloat(20)
        } get {
            return Int(self.layer.cornerRadius)
        }
    }
    
    @IBInspectable var buttonHeight : CGFloat {
        set {
            self.frame.size.height = CGFloat(40)
        } get {
            return CGFloat(self.frame.size.height)
        }
    }
    
}

