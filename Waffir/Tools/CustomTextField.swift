//
//  CustomTextField.swift
//  AMDA
//
//  Created by Abdalrahim Abdulah on 29/01/2018.
//  Copyright © 2018 Tima_Shams. All rights reserved.
//

import UIKit

/// Designable class for text field
@IBDesignable class CustomTextField : UITextField {
    
    /// Custom the corner radius
    @IBInspectable var cornerRadius : Int {
        set {
            self.layer.cornerRadius = CGFloat(20)
        } get {
            return Int(self.layer.cornerRadius)
        }
    }
    
    /// Custom the color of the border
    @IBInspectable var borderColor : UIColor = UIColor.white {
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
}

