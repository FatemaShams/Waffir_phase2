//
//  CollectionCell.swift
//  CollectionInTable
//
//  Created by Adi Nugroho on 2/22/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit

class CollectionCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = UIEdgeInsetsMake(
                SectionasConstant.offset,    // top
                SectionasConstant.offset,    // left
                SectionasConstant.offset,    // bottom
                SectionasConstant.offset     // right
            )
            
            layout.minimumInteritemSpacing = SectionasConstant.minItemSpacing
            layout.minimumLineSpacing = SectionasConstant.minLineSpacing
        }
        
        collectionView.isScrollEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
}


var sectionImages = [ UIImage(named: "sec1") ,  UIImage(named: "sec2") ,  UIImage(named: "sec3") ,  UIImage(named: "sec4") ]


extension CollectionCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(SectionasConstant.totalItem)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "singleSectionCell", for: indexPath) as! singleSectionCell
        
        
        cell.sectionImage.image = sectionImages[indexPath.row]
        cell.sectionTitle.text = "قسم التجميل "
        cell.sectionTitle.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        return cell
    }
}

extension CollectionCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemWidth = SectionasConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
        
        return CGSize(width: itemWidth, height: itemWidth)
    }
}
