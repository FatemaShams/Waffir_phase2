//
//  singleSectionCell.swift
//  Waffir
//
//  Created by tima shams on 24/05/1439 AH.
//  Copyright © 1439 confrontstar. All rights reserved.
//

import UIKit

class singleSectionCell: UICollectionViewCell {
    
    @IBOutlet weak var sectionImage: UIImageView!
    @IBOutlet weak var sectionTitle: UILabel!
    
}
