//
//  ItemsCell.swift
//  Waffir
//
//  Created by tima shams on 24/05/1439 AH.
//  Copyright © 1439 confrontstar. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {


    @IBOutlet weak var collectionView: UICollectionView!
    var test : MainViewController!

    override func awakeFromNib() {
        super.awakeFromNib()

        //collectionView.isScrollEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.scrollsToTop = true
        collectionView.showsHorizontalScrollIndicator = false

        
        // Initialization code
    }

    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}




var images = [ UIImage(named: "drink1") ,  UIImage(named: "drink2") ,  UIImage(named: "drink3") ,  UIImage(named: "drink4") ]

extension ItemCell: UICollectionViewDataSource   {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Int(images.count)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "singleItemCell", for: indexPath) as! singleItemCell
        
        cell.itemImage.image = images[indexPath.row]
        cell.itemImage.contentMode = .scaleAspectFit
        cell.itemDescription.text = "مشروب غازي ممتاز"
        cell.itemPrice.text = "20$"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        test.performSegue(withIdentifier: "test", sender: nil)
    }
    
    
}





extension ItemCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
      //  let itemWidth = ItemsConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
        
        return CGSize(width: 170 , height: 265 )
    }
    
    
    
}


