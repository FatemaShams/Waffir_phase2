//
//  HeaderCell.swift
//  Waffir
//
//  Created by tima shams on 24/05/1439 AH.
//  Copyright © 1439 confrontstar. All rights reserved.
//

import UIKit

class HeaderCell: UITableViewCell {

    
    var imageInt = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageInt = 1
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector:  #selector(AutoScroll) , userInfo: nil, repeats: true)
        // Initialization code
        
        imageview.contentMode = .scaleAspectFill
    }

    
    
    
    
    @objc func AutoScroll(){
        
        if(imageInt == 4)
        {
            imageInt = 1
        }
        else
        {imageInt += 1
        }
        
        self.imageGallery()
        
        
    }
    
    
    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var right: UIButton!
    
    @IBOutlet weak var left: UIButton!
    
    
    
    
    
    @IBAction func prevAction(_ sender: Any) {
        
        imageInt -= 1
        self.imageGallery()
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        imageInt += 1
        self.imageGallery()
        
        
    }
    

    
    func imageGallery() {
        
        if(imageInt == 1)
        {
            left.isEnabled = true
            right.isEnabled = false
            imageview.image = UIImage(named : "add1")
            
        }
        if(imageInt == 2)
        {
            imageview.image = UIImage(named : "add2")
            left.isEnabled = true
            right.isEnabled = true
        }
        
        if(imageInt == 3)
        {
            imageview.image = UIImage(named : "add3")
            left.isEnabled = true
            right.isEnabled = true
        }
        
        if(imageInt == 4)
        {
            imageview.image = UIImage(named : "add4")
            left.isEnabled = false
            right.isEnabled = true
        }
        
        
        
    }
    
    
    
    
    
    
}
