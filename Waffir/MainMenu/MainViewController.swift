//
//  ViewController.swift
//  CollectionInTable
//
//  Created by Adi Nugroho on 2/22/17.
//  Copyright © 2017 Lonely Box. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    


    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var sideBarItem: UIBarButtonItem!
    @IBOutlet weak var cartItem: UIBarButtonItem!
    
    @IBOutlet weak var search: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sideBarItem.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        cartItem.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        search.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        

        tableView.dataSource = self
        tableView.delegate = self
        tableView.showsVerticalScrollIndicator = false
    }
    
}




extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell", for: indexPath)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTitle", for: indexPath)
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! ItemCell
            cell.test = self 
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SectionTitle", for: indexPath)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectionCell", for: indexPath) as! CollectionCell
            return cell
        }
    }
}

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 200
        case 1:
            return UITableViewAutomaticDimension
        case 2:
            return 280
        case 3:
            return UITableViewAutomaticDimension
        case 4:
            let itemHeight = SectionasConstant.getItemWidth(boundWidth: tableView.bounds.size.width)
            
            let totalRow = ceil(SectionasConstant.totalItem / SectionasConstant.column)
            
            let totalTopBottomOffset = SectionasConstant.offset + SectionasConstant.offset
            
            let totalSpacing = CGFloat(totalRow - 1) * SectionasConstant.minLineSpacing
            
            let totalHeight  = ((itemHeight * CGFloat(totalRow)) + totalTopBottomOffset + totalSpacing)
            
            return totalHeight
        default:
            return UITableViewAutomaticDimension
        }
    }
}

