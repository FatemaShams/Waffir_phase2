


import UIKit

class SectionasConstant {
    static let totalItem: CGFloat = CGFloat(images.count)
    
    static let column: CGFloat = 2
    
    static let minLineSpacing: CGFloat = 5
    static let minItemSpacing: CGFloat = 5
    
    static let offset: CGFloat = 2 // TODO: for each side, define its offset
    
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        
        // totalCellWidth = (collectionview width or tableview width) - (left offset + right offset) - (total space x space width)
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        
        return totalWidth / column
    }
}



class ItemsConstant {
    
    
    static let totalItem: CGFloat = 10
    
    static let column: CGFloat = 1
    
    static let minLineSpacing: CGFloat = 5
    static let minItemSpacing: CGFloat = 5
    
    static let offset: CGFloat = 5 // TODO: for each side, define its offset
    
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        
        // totalCellWidth = (collectionview width or tableview width) - (left offset + right offset) - (total space x space width)
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        
        return totalWidth / column
    }
}
