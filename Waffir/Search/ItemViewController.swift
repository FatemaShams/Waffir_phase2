//
//  ItemViewController.swift
//  Waffir
//
//  Created by tima shams on 26/05/1439 AH.
//  Copyright © 1439 confrontstar. All rights reserved.
//

import UIKit

class ItemViewController: UIViewController {

    

    @IBAction func addToCart(_ sender: Any) {
        
        
        
    }
    
    
    @IBOutlet weak var noOfItemsInCart: UILabel!
    
    @IBAction func addItem(_ sender: Any) {
        
        let temp = Int(noOfItemsInCart.text!)
        noOfItemsInCart.text = "\(temp! + 1)"
        
        
    }
    
   
    @IBAction func removeItem(_ sender: Any) {
        
        
        let temp = Int(noOfItemsInCart.text!)
        
        if(temp != 0 )
        {  noOfItemsInCart.text = "\(temp! - 1)" }
        
        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()

        
        // Do any additional setup after loading the view.
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


extension ItemViewController : UITableViewDataSource , UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemImageCell", for: indexPath) 
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemInfoCell", for: indexPath)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ItemRecommendationCell", for: indexPath)
            return cell
        default:
            return UITableViewCell()
        }
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 200
        case 1:
            return 250
        case 2:
            return 200
        default:
            return UITableViewAutomaticDimension
        }
    
    }
    
    
    
}


