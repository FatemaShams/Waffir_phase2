//
//  MainMenuViewController.swift
//  Waffir
//
//  Created by MacBook Pro on 05/02/2018.
//  Copyright © 2018 confrontstar. All rights reserved.
//

import UIKit
import Auk


// https://github.com/PhilippeBoisney/ModernSearchBar



class SearchMenuViewController: UIViewController , UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    
    @IBOutlet weak var searchbar: UISearchBar!
    
    @IBOutlet weak var table: UITableView!
    
    var animalArray = [Animal]()
    var currentAnimalArray = [Animal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchbar.layer.borderWidth = 1
        searchbar.layer.borderColor = #colorLiteral(red: 0.8325922489, green: 0.2186652422, blue: 0.4138093889, alpha: 1)
        table.estimatedRowHeight = 50
        setUpAnimals()
        setUpSearchBar()
        table.tableFooterView = UIView()
        table.tableHeaderView = UIView()


    }
    
    
    
    func setUpSearchBar() {

        searchbar.delegate = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return  currentAnimalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? TableCell else {
            
            return UITableViewCell()
            
        }
        
        cell.nameLbl.text = currentAnimalArray[indexPath.row].name
        cell.imgView.image  = UIImage(named : currentAnimalArray[indexPath.row].image )
        
        return cell
    }
    


    


    
    private func setUpAnimals() {
        // CATS
        animalArray.append(Animal(name: "pepsi", category: .cat, image:"profile"))
        animalArray.append(Animal(name: "coca cola", category: .cat, image:"profile"))
        
        // DOGS
        animalArray.append(Animal(name: "twix", category: .dog, image:"profile"))
        animalArray.append(Animal(name: "paprika", category: .dog, image:"profile"))
        
        
        currentAnimalArray = animalArray
    }

    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        guard !searchText.isEmpty else { currentAnimalArray = animalArray
            
            table.reloadData()
            return  }
        
        currentAnimalArray = animalArray.filter({    animal -> Bool in
            
            guard let text = searchbar.text else {return false}
            return animal.name.lowercased().contains(text.lowercased())
        })
        
        table.reloadData()
    }
    
    
  
    


}




class Animal {
    let name: String
    let image: String
    let category: AnimalType
    
    init(name: String, category: AnimalType, image: String) {
        self.name = name
        self.category = category
        self.image = image
    }
}

enum AnimalType: String {
    case cat = "Cat"
    case dog = "Dog"
}







