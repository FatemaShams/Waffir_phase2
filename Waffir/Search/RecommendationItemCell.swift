//
//  RecommendationItemCell.swift
//  Waffir
//
//  Created by tima shams on 26/05/1439 AH.
//  Copyright © 1439 confrontstar. All rights reserved.
//

import UIKit

class RecommendationItemCell: UICollectionViewCell {
    
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    
    
}
