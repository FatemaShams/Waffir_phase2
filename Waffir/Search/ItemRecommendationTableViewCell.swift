//
//  ItemRecommendationTableViewCell.swift
//  Waffir
//
//  Created by tima shams on 26/05/1439 AH.
//  Copyright © 1439 confrontstar. All rights reserved.
//

import UIKit

class ItemRecommendationTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.scrollsToTop = true
        collectionView.showsHorizontalScrollIndicator = false
    
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


extension ItemRecommendationTableViewCell : UICollectionViewDataSource , UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecommendationItemCell", for: indexPath) as! RecommendationItemCell
        
        
        cell.itemImage.image = UIImage(named: "drink1")
        cell.itemPrice.text = "s2sd"
        cell.itemDescription.text = "sdfsdf"
        
        
        return cell 
        
    
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    
    
}

